package main;

import controlador.Controlador;
import modelo.CalendarioJSON;
import modelo.EquiposJSON;
import vista.VentanaPrincipal;

public class Main {

	public static void main(String[] args){
		CalendarioJSON calendario= CalendarioJSON.leerJSON("././recursos/archivos/calendario.json"); //leo el archivo json
		EquiposJSON listaDeEquipos= EquiposJSON.leerJSON("././recursos/archivos/listaDeEquipos.json");
		VentanaPrincipal ventana= new VentanaPrincipal();
		Controlador controlador= new Controlador(ventana);
		controlador.cargarCalendarioAlTorneo(calendario.getCalendario(), listaDeEquipos.getListaDeEquipos());
		controlador.mostrarVentanaPrincipal();
	}

}
