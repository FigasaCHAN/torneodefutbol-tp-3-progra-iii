package modelo;

import java.io.Serializable;
import java.util.Objects;

public class Partido implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Equipo equipoLocal;
	private Equipo equipoVisitante;
	private Arbitro arbitro;
	public Partido(Equipo equipoLocal, Equipo equipoVisitante) {
		validarEquipos(equipoLocal, equipoVisitante);
		this.equipoLocal= equipoLocal;
		this.equipoVisitante= equipoVisitante;
		this.arbitro = null; //inicia sin arbitro
	}
	public Equipo getEquipoLocal() {
		return equipoLocal;
	}
	public void setEquipoLocal(Equipo equipoLocal) {
		this.equipoLocal = equipoLocal;
	}
	public Equipo getEquipoVisitante() {
		return equipoVisitante;
	}
	public void setEquipoVisitante(Equipo equipoVisitante) {
		this.equipoVisitante = equipoVisitante;
	}
	
	public Arbitro getArbitro() {
		return arbitro;
	}
	public void setArbitro(Arbitro arbitro) {
		this.arbitro = arbitro;
	}
	private void validarEquipos(Equipo equipoLocal, Equipo equipoVisitante) {
		if(equipoLocal==null) {
			throw new IllegalArgumentException("El equipo local es null");
		}
		if(equipoVisitante==null) {
			throw new IllegalArgumentException("El equipo local es null");
		}
		if(equipoLocal.equals(equipoVisitante)) {
			throw new IllegalArgumentException("Ambos equipos son iguales");
		}
	}
	@Override
	public int hashCode() {
		return Objects.hash(equipoLocal, equipoVisitante);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Partido other = (Partido) obj;
		return Objects.equals(equipoLocal, other.equipoLocal) && Objects.equals(equipoVisitante, other.equipoVisitante);
	}
	@Override
	public String toString() {
		return "[" + equipoLocal + " vs " + equipoVisitante +  " || Arbitro= " + arbitro + "]";
	}
	
}
