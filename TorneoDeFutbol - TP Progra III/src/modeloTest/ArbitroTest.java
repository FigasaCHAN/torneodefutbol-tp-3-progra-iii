package modeloTest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import modelo.Arbitro;

public class ArbitroTest {

	@Test (expected= IllegalArgumentException.class)
	public void arbitroInvalido() {
		new Arbitro(null);
	}
	@Test 
	public void arbitroValido() {//es valido un arbitro con nombre vacio
		Arbitro test= new Arbitro("");
		assertEquals("", test.getNombre());
	}
}
