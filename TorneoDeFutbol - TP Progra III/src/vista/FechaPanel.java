package vista;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import java.util.ArrayList;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JTable;
import java.awt.Font;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import modelo.Partido;

@SuppressWarnings("serial")
public class FechaPanel extends JPanel {
	private JTable tabla;
	private JLabel cartelFecha;
	private final Color COLOR_CARTEL_FECHA= new Color (35,190,184);
	private final Color COLOR_BORDE_CARTEL_FECHA=new Color (2,163,156);
	private final Color CABECERA_COLOR= new Color(109,117,205);
	private JScrollPane scroll;

	public FechaPanel(String fecha) {
		this.setLayout(null);
		
		this.tabla = new JTable();
		this.setBounds(0, 0, 785, 470);
		propiedadesATabla();
		
		this.scroll = new JScrollPane(tabla); //creo un scroll pane y asocio a la tabla con el scroll
		this.scroll.setBounds(0, 42, 785, 428);//las dimensiones del area donde se encuentra el scroll
		this.add(scroll);
		
		
		this.cartelFecha = new JLabel("Fecha: " + fecha);
		propiedadesDeCartel();
		add(cartelFecha);
		

	}
	public void cargarFechaEnTabla(ArrayList<Partido> partidos) {
		String [][] matrizDePartidos = new String[partidos.size()][3]; //cada elem va a tener 3 datos
		//{equipoLocal,equipoVisitante,arbitro}
		for(int i= 0; i<matrizDePartidos.length;i++) {
			Partido partido= partidos.get(i);
			if(partido.getArbitro()!=null) {
				matrizDePartidos[i] = new String[] { partido.getEquipoLocal().getNombre(), partido.getEquipoVisitante().getNombre(), partido.getArbitro().getNombre()};
			}
			else {
				matrizDePartidos[i] = new String[] { partido.getEquipoLocal().getNombre(), partido.getEquipoVisitante().getNombre(), null};
			}
			
		}
		tabla.setModel(new DefaultTableModel(
				matrizDePartidos,
				new String[] {
					"Equipo Local", "Equipo Visitante", "Arbitro"
				}
			) {
				@SuppressWarnings("rawtypes")
				Class[] columnTypes = new Class[] {
					String.class, String.class, String.class
				};
				
				@SuppressWarnings({ "unchecked", "rawtypes" })
				public Class getColumnClass(int columnIndex) {
					return columnTypes[columnIndex];
				}
				boolean[] columnEditables = new boolean[] {
					false, false, false
				};
				public boolean isCellEditable(int row, int column) {
					return columnEditables[column];
				}
			});
		
		propiedadesATabla();

		
	}
	private void propiedadesATabla() {
		this.tabla.setFillsViewportHeight(true);
		this.tabla.setBounds(0, 42, 785, 428);
		this.tabla.setRowHeight(50);//alto de las filas
		
		
		/*Alinear tabla */
		DefaultTableCellRenderer tcr = new DefaultTableCellRenderer();
		tcr.setHorizontalAlignment(SwingConstants.CENTER);
		for(int i= 0; i< tabla.getColumnCount() ;i++ ) { 
			tabla.getColumnModel().getColumn(i).setCellRenderer(tcr);
		}
		
		/*para que no se puedan seleccionar las celdas*/
		this.tabla.setCellSelectionEnabled(false);
		/* propiedades de dise�o*/
		this.tabla.setForeground(new Color(0,0,0));
		this.tabla.setFont(new Font("Tahoma", Font.BOLD, 16));
		this.tabla.setBackground(new Color(255,227,204));
		propiedadesDeCabecera();

	}
	private void propiedadesDeCabecera() {
		this.tabla.getTableHeader().setReorderingAllowed(false); //esto es para bloquear las columnas
		this.tabla.getTableHeader().setPreferredSize(new Dimension(0, 50));//las dimensiones de la cabecera
		this.tabla.getTableHeader().setBackground(CABECERA_COLOR);
		this.tabla.getTableHeader().setFont(new Font("Tahoma", Font.BOLD, 16) );
		this.tabla.getTableHeader().setForeground(new Color(255,255,255));
	}
	private void propiedadesDeCartel() {
		this.cartelFecha.setForeground(Color.WHITE);
		this.cartelFecha.setFont(new Font("Tahoma", Font.BOLD, 16));
		this.cartelFecha.setHorizontalAlignment(SwingConstants.CENTER);

		this.cartelFecha.setBounds(0, 0, 785, 42);
		this.cartelFecha.setOpaque(true);
		this.cartelFecha.setBackground(this.COLOR_CARTEL_FECHA);
		this.cartelFecha.setBorder(new LineBorder(this.COLOR_BORDE_CARTEL_FECHA, 6));
	}
	public void mostrar(boolean visibilidad) {
		this.setVisible(visibilidad);
	}
}
