package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;

import modelo.Equipo;
import modelo.Fecha;
import modelo.Torneo;
import vista.VentanaPrincipal;

public class Controlador {
	private VentanaPrincipal ventana;
	private JButton btnFechaAnterior;
	private JButton btnAsignarArbitraje;
	private JButton btnFechaSiguiente;
	private JButton btnCargarArbitro;
	private JButton btnCargarEnVCA; //boton que se encuentra en la ventana cargar arbitro
	private JButton btnCancelarEnVCA; //boton que se encuentra en la ventana cargar arbitro
	private Torneo torneo;
	private boolean primerClicEnBtnArbitro; //esto realizar la asignacion una unica vez
	public Controlador (VentanaPrincipal ventana) {
		this.ventana = ventana;
		this.torneo=null;
		inicializarBotones();
		this.primerClicEnBtnArbitro= false;
		agregarEventos();
	}
	public void mostrarVentanaPrincipal() {
		this.ventana.mostrar();
	}
	public void cargarCalendarioAlTorneo(ArrayList<Fecha> calendario, ArrayList<Equipo> equipos) {
		this.torneo= new Torneo(calendario,equipos);
		this.ventana.cargarTorneo(calendario);
		this.ventana.setLimiteDeArbitrosEnVentanaCargarArbitro(equipos.size()/2);//pongo limite en valor para la ventana emergente
	}
	private void inicializarBotones() {
		this.btnFechaAnterior=this.ventana.getBtnFechaAnterior();
		this.btnAsignarArbitraje=this.ventana.getBtnAsignarArbitraje();
		this.btnFechaSiguiente=this.ventana.getBtnFechaSiguiente();
		this.btnCargarArbitro=this.ventana.getBtnCargarArbitro();
		this.btnCargarEnVCA= this.ventana.getBtnVentanaDeCargaArbitro__CargarArbitro();
		this.btnCancelarEnVCA=this.ventana.getBtnVentanaDeCargaArbitro__Cancelar();
	}
	private void agregarEventos() {
		btnFechaAnterior.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventana.paginaAnterior();
			}
		});
		btnAsignarArbitraje.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(!primerClicEnBtnArbitro) {
					torneo.asignarArbitraje();//asigna el arbitraje al torneo
					primerClicEnBtnArbitro=true;
					btnCargarArbitro.setEnabled(true); //se habilita
				}
				else {
					torneo.mezclarArbitraje(); //mezcla el arbitraje, ya que solucion hay una sola 
				}
					ventana.cargarTorneo(torneo.getFechas()); //carga otra vez el torneo
				
			}
		});
		btnFechaSiguiente.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventana.paginaSiguiente();
			}
		});
		btnCargarArbitro.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventana.mostrarVentanaDeCargarArbitro(true);
			}
		});
		this.btnCancelarEnVCA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ventana.mostrarVentanaDeCargarArbitro(false);
			}
		}); 
		this.btnCargarEnVCA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				torneo.cambiarNombreAArbitro(ventana.idDeArbitro(), ventana.nombreDeArbitro());
				ventana.mostrarVentanaDeCargarArbitro(false);
				ventana.cargarTorneo(torneo.getFechas()); //carga otra vez el torneo

			}
		});
	}
}
